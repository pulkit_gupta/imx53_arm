/*
 * main.cpp
 *
 *  Created on: Aug 19, 2013
 *      Author: embedded
 */


//============================================================================
// Name        : Funtions.cpp
// Author      : pulkit
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <sys/time.h>
#include <stdio.h>



//time measurement
struct timeval start,ending;
long mtime, seconds, useconds;

using namespace std;
using namespace cv;

void startTimeMeasurement()
{
   gettimeofday(&start, NULL);
}

void stopTimeMeasurement()
{
   gettimeofday(&ending, NULL);

    seconds  = ending.tv_sec  - start.tv_sec;
    useconds = ending.tv_usec - start.tv_usec;

    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    printf("Elapsed Time %ld ms\n", mtime);
}


/* Function name: toGray(Mat)
 * Operation: Take input RGB image of Mat type
 * returns: Mat type Grayscale image
 */

Mat toGray(Mat image)
{
	startTimeMeasurement();
	Mat gray_image(image.size(),CV_8UC1);  //declaration of empty matrix header for storing
	cvtColor( image, gray_image, CV_BGR2GRAY ); //converting
	stopTimeMeasurement();  //time to calculate the conversion time by opencv library
	return gray_image;
}

/* Function name: createHis(Mat)
 * Operation: Take input RGB or Grayscale image of Mat type
 * returns: Mat type image containing Histogram
 */
Mat createHis(Mat src)
{
	startTimeMeasurement();
	vector<Mat> bgr_vec;
	  split( src, bgr_vec );
	  bool isRGB = true;

	  if(bgr_vec[1].empty())
	  {
		  cout<<"Image is not RGB\n";
		  isRGB = false;
	  }
	  /// Establish the number of bins. In this case 256 bins and dim =1
	  int histSize = 256;
	  /// Set the ranges ( for B,G,R) )
	  float range[] = { 0, 256 } ;
	  const float* histRange = { range };

	  bool uniform = true; bool accumulate = false;

	  Mat b_hist, g_hist, r_hist;

	  /// Compute the histograms:
	  calcHist( &bgr_vec[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );
	  if(isRGB)
	  {
		  calcHist( &bgr_vec[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate );
		  calcHist( &bgr_vec[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate );
	  }


	  // Draw the histograms for B, G and R
	  int width_his = 412; int height_his = 200;
	  int bin_w = cvRound( (double) width_his/histSize );

	  Mat hist_mat( height_his, width_his, CV_8UC3, Scalar( 0,0,0) );

	  /// Normalize the result to [ 0, hist_mat.rows ]
	  normalize(b_hist, b_hist, 0, hist_mat.rows, NORM_MINMAX, -1, Mat() );
	  if(isRGB)
	  {
		  normalize(g_hist, g_hist, 0, hist_mat.rows, NORM_MINMAX, -1, Mat() );
		  normalize(r_hist, r_hist, 0, hist_mat.rows, NORM_MINMAX, -1, Mat() );
	  }


	  /// Draw for each channel
	  for( int i = 1; i < histSize; i++ )
	  {
	      line( hist_mat, Point( bin_w*(i-1), height_his - cvRound(b_hist.at<float>(i-1)) ) ,
	                       Point( bin_w*(i), height_his - cvRound(b_hist.at<float>(i)) ),
	                       Scalar( 255, 0, 0), 2, 8, 0  );
	      if(isRGB)
	      {
	    	  line( hist_mat, Point( bin_w*(i-1), height_his - cvRound(g_hist.at<float>(i-1)) ) ,
	                       Point( bin_w*(i), height_his - cvRound(g_hist.at<float>(i)) ),
	                       Scalar( 0, 255, 0), 2, 8, 0  );

	    	  line( hist_mat, Point( bin_w*(i-1), height_his - cvRound(r_hist.at<float>(i-1)) ) ,
	                       Point( bin_w*(i), height_his - cvRound(r_hist.at<float>(i)) ),
	                       Scalar( 0, 0, 255), 2, 8, 0  );
	      }
	  }
	  stopTimeMeasurement();
	  return hist_mat;
}

/* Function name: correctGamma(Mat, double)
 * Operation: Take input img of Mat type and value of gamma of the device by which image has been taken.
 * returns: Mat type image with corrected gamma
 */
Mat correctGamma( Mat& img, double gamma )
{
	double inverse_gamma = 1.0 / gamma;
	Mat lut_matrix(1, 256, CV_8UC1 );
	uchar * ptr = lut_matrix.ptr();
	for( int i = 0; i < 256; i++ )
		ptr[i] = (int)( pow( (double) i / 255.0, inverse_gamma ) * 255.0 );
	Mat result;
	LUT( img, lut_matrix, result );
	return result;
}

int main(int argc, char* argv[])
{

	Mat image;
	image = imread(argv[1],CV_LOAD_IMAGE_UNCHANGED);
	if( argc != 2 || !image.data )
	{
		printf( "No image data \n " );
		return -1;
	}
	string imgname = argv[1];
	imgname = imgname.substr(0,3); //to distinguish between different input image results.
	double gam= 0;
	int choice=0;
	cout<< "\n\nEnter Your choice of operation: \n\n 1. RGB to Grayscale\n 2. Create Histogram\n 3. Gamma correction\n\n choice:";
	cin>>choice;
	switch(choice)
	{
	case 1:
		cout<<"converting image into grayscale.\n";
		imwrite(imgname+"_gray.jpg",toGray(image)); //writing to disk
		cout<<"Your result has been wrote into the file named: "<<imgname+"_gray.jpg"<<endl;
		break;
	case 2:
		cout<<"Creating Histogram.\n";
		imwrite(imgname+"_His.jpg",createHis(image)); //writing to disk
		cout<<"Your result has been wrote into the file named: "<<imgname+"_His.jpg"<<endl;
		break;

	case 3:
		cout<<"Correcting Gamma.\n Please enter the value of gamma of the device by which image has been taken (For example 2.2) :";
		cin>>gam;
		imwrite(imgname+"gamma_corrected.jpg",correctGamma(image, gam));
		cout<<"Your result has been wrote into the file named: "<<"imgname+"gamma_corrected.jpg";
		break;
	case 4:
		cout<<"";
		break;
	default:
		break;
	}
	//usleep(10);
	return 0;
}
