/*
 * ImageDisplay.cpp
 *
 *  Created on: Jun 14, 2013
 *      Author: embedded
 */

#include "ImageDisplay.h"

ImageDisplay::ImageDisplay() :
//Initialisierungsliste
	fd_fb_alloc(0),
	screen_size(0),
	ret(0),
	fd_fb(0),
	size(0),
	i(0),
	k(0),
	done_cnt(0),
	fcount(0)
{

	for(int k= 0; k <BUF_CNT;k++)
	{
		buf[k] = 0;
		paddr[k] = 0;
	}
	//initialize ipu-handles
	memset(&ipu_handle, 0, sizeof(ipu_lib_handle_t));
	memset(&test_handle, 0, sizeof(ipu_test_handle_t));
	test_handle.ipu_handle = &ipu_handle;
	test_handle.mode = OP_STREAM_MODE;
	test_handle.block_width = 80;

	startIPU(&test_handle);
}

ImageDisplay::~ImageDisplay() {
	closeIPU(&test_handle);
}

void ImageDisplay::setImage(cv::Mat image, void* ResizedImages[3])
{
	memcpy(buf[1],(void*)image.data,1280 * 1024);
	updateIPU(&test_handle);
	ResizedImages[0] = fb[0];
	ResizedImages[1] = fb[1];
	ResizedImages[2] = fb[2];

	cv::imwrite("img1.bmp", cv::Mat(1280, 1024, CV_8UC1, ResizedImages[0], 3));
	cv::imwrite("img2.bmp", cv::Mat(640, 512, CV_8UC1, ResizedImages[1], 3));
	cv::imwrite("img3.bmp", cv::Mat(320, 256, CV_8UC1, ResizedImages[2], 3));
}

void ImageDisplay::setImage(void* buffer, void* ResizedImages[3])
{
	memcpy(buf[0],buffer,1280 * 1024);
	updateIPU(&test_handle);
	ResizedImages[0] = fb[0];
	ResizedImages[1] = fb[1];
	ResizedImages[2] = fb[2];
}


int ImageDisplay::startIPU(ipu_test_handle_t * test_handle)
{
	if ((fd_fb = open("/dev/fb0", O_RDWR, 0)) < 0) {
		printf("Unable to open /dev/fb0\n");
		ret = -1;
	}

	g_alpha.alpha = 128;
	g_alpha.enable = 1;
	if (ioctl(fd_fb, MXCFB_SET_GBL_ALPHA, &g_alpha) < 0) {
		printf("Set global alpha failed\n");
		ret = -1;

	}

	if ( ioctl(fd_fb, FBIOGET_VSCREENINFO, &fb_var) < 0) {
		printf("Get FB var info failed!\n");
		ret = -1;

	}
	if ( ioctl(fd_fb, FBIOGET_FSCREENINFO, &fb_fix) < 0) {
		printf("Get FB fix info failed!\n");
		ret = -1;

	}

	if(fb_var.yres_virtual != 3*fb_var.yres)
	{
		fb_var.yres_virtual = 3*fb_var.yres;
		if ( ioctl(fd_fb, FBIOPUT_VSCREENINFO, &fb_var) < 0) {
			printf("Get FB var info failed!\n");
			ret = -1;

		}
	}

	screen_size = fb_var.yres * fb_fix.line_length;
	fb[0] = mmap(NULL, 3 * 1280 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED,
			fd_fb, 0);
	if (fb[0] == MAP_FAILED) {
		printf("fb buf0 mmap failed, errno %d!\n", errno);
		ret = -1;

	}
	/* ipu use fb base+screen_size as buf0 */
	fb[0] = (void *)((char *)fb[0] + 1208 * 1024);
	fb[1] = (void *)((char *)fb[0] + (1208 * 1024)/4);
	fb[2] = (void *)((char *)fb[0] + (1208 * 1024)/16);

	/* use I420 input format as fix*/
	test_handle->mode = OP_STREAM_MODE;
	test_handle->fcount = fcount = 200;
	test_handle->input.width = 1280;
	test_handle->input.height = 1024;
	test_handle->input.fmt = v4l2_fourcc('I', '4', '2', '0');
	if (fb_var.bits_per_pixel == 24)
		test_handle->output.fmt = v4l2_fourcc('B', 'G', 'R', '3');
	else
		test_handle->output.fmt = v4l2_fourcc('R', 'G', 'B', 'P');
	test_handle->output.show_to_fb = 1;
	test_handle->output.fb_disp.fb_num = 0;
	test_handle->output.rot = 3;

	/* one output case -- full screen */
	test_handle->output.width = 640;
	test_handle->output.height = 512;


	/*allocate dma buffers from fb dev*/
	size = test_handle->input.width * test_handle->input.height * 3/2;
	ret = dma_memory_alloc(size, BUF_CNT, paddr, buf);
	if ( ret < 0) {
		printf("dma_memory_alloc failed\n");
	}
	/* we are using stream mode and we set dma addr by ourselves*/
	test_handle->input.user_def_paddr[0] = paddr[0];
	test_handle->input.user_def_paddr[1] = paddr[1];
	test_handle->input.user_def_paddr[2] = paddr[2];

	gen_fill_pattern((char*)buf[0], 1280, 1024);
	gen_fill_pattern((char*)buf[1], 640, 512);
	gen_fill_pattern((char*)buf[2], 320, 256);


	done_cnt = i = 1;
	ret = mxc_ipu_lib_task_init(&(test_handle->input), NULL, &(test_handle->output), test_handle->mode, test_handle->ipu_handle);

	if (ret < 0) {
		printf("mxc_ipu_lib_task_init failed!\n");

	}

	return 0;
}

int ImageDisplay::updateIPU(ipu_test_handle_t * test_handle)
{
	if(mxc_ipu_lib_task_buf_update(test_handle->ipu_handle, paddr[1], 0, 0, NULL, NULL) < 0)
		return -1;
	else
		return 0;
}

int ImageDisplay::closeIPU(ipu_test_handle_t * testhandle)
{
	mxc_ipu_lib_task_uninit(testhandle->ipu_handle);
	dma_memory_free(size, BUF_CNT, paddr, buf);
	if (fd_fb)
		close(fd_fb);

	return 0;
}

int ImageDisplay::dma_memory_alloc(int size, int cnt, dma_addr_t paddr[], void * vaddr[])
{
	int i, ret = 0;

	if ((fd_fb_alloc = open("/dev/fb0", O_RDWR, 0)) < 0) {
		printf("Unable to open /dev/fb0\n");
		ret = -1;
		goto done;
	}

	for (i=0;i<cnt;i++) {
		/*alloc mem from DMA zone*/
		/*input as request mem size */
		paddr[i] = size;
		if ( ioctl(fd_fb_alloc, FBIO_ALLOC, &(paddr[i])) < 0) {
			printf("Unable alloc mem from /dev/fb0\n");
			close(fd_fb_alloc);
			if ((fd_fb_alloc = open("/dev/fb1", O_RDWR, 0)) < 0) {
				printf("Unable to open /dev/fb1\n");
				if ((fd_fb_alloc = open("/dev/fb2", O_RDWR, 0)) < 0) {
					printf("Unable to open /dev/fb2\n");
					ret = -1;
					goto done;
				} else if ( ioctl(fd_fb_alloc, FBIO_ALLOC, &(paddr[i])) < 0) {
					printf("Unable alloc mem from /dev/fb2\n");
					ret = -1;
					goto done;
				}
			} else if ( ioctl(fd_fb_alloc, FBIO_ALLOC, &(paddr[i])) < 0) {
				printf("Unable alloc mem from /dev/fb1\n");
				close(fd_fb_alloc);
				if ((fd_fb_alloc = open("/dev/fb2", O_RDWR, 0)) < 0) {
					printf("Unable to open /dev/fb2\n");
					ret = -1;
					goto done;
				} else if ( ioctl(fd_fb_alloc, FBIO_ALLOC, &(paddr[i])) < 0) {
					printf("Unable alloc mem from /dev/fb2\n");
					ret = -1;
					goto done;
				}
			}
		}

		vaddr[i] = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED,
				fd_fb_alloc, paddr[i]);
		if (vaddr[i] == MAP_FAILED) {
			printf("mmap failed!\n");
			ret = -1;
			goto done;
		}
	}
done:
	return ret;
}

void ImageDisplay::dma_memory_free(int size, int cnt, dma_addr_t paddr[], void * vaddr[])
{
	int i;

	for (i=0;i<cnt;i++) {
		if (vaddr[i])
			munmap(vaddr[i], size);
		if (paddr[i])
			ioctl(fd_fb_alloc, FBIO_FREE, &(paddr[i]));
	}
}

void ImageDisplay::gen_fill_pattern(char * buf, int in_width, int in_height)
{
	int y_size = in_width * in_height;
	int h_step = in_height / 16;
	int w_step = in_width / 16;
	int h, w;
	uint32_t y_color = 0;
	int32_t u_color = 0;
	int32_t v_color = 0;
	uint32_t rgb = 0;
	static int32_t alpha = 0;
	static int inc_alpha = 1;

	for (h = 0; h < in_height; h++) {
		int32_t rgb_temp = rgb;

		for (w = 0; w < in_width; w++) {
			if (w % w_step == 0) {
				y_color = y(rgb_temp);
				y_color = (y_color * alpha) / 255;

				u_color = u(rgb_temp);
				u_color = (u_color * alpha) / 255;
				u_color += 128;

				v_color = v(rgb_temp);
				v_color = (v_color * alpha) / 255;
				v_color += 128;

				rgb_temp++;
				if (rgb_temp > 255)
					rgb_temp = 0;
			}
			buf[(h*in_width) + w] = y_color;
			if (!(h & 0x1) && !(w & 0x1)) {
				buf[y_size + (((h*in_width)/4) + (w/2)) ] = u_color;
				buf[y_size + y_size/4 + (((h*in_width)/4) + (w/2))] = v_color;
			}
		}
		if ((h > 0) && (h % h_step == 0)) {
			rgb += 16;
			if (rgb > 255)
				rgb = 0;
		}

	}
	if (inc_alpha) {
		alpha+=4;
		if (alpha >= 255) {
			inc_alpha = 0;
		}
	} else {
		alpha-=4;
		if (alpha <= 0) {
			inc_alpha = 1;
		}
	}
}


