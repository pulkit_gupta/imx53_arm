
//============================================================================
// Name        : Funtions.cpp
// Author      : Pulkit
// Version     :
// Copyright   : Your copyright notice
// Description : c++ cross compiled with arm linux
//============================================================================


#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <sys/time.h>
#include <stdio.h>



//time measurement
struct timeval start,ending;
long mtime, seconds, useconds;

using namespace std;
using namespace cv;

void startTimeMeasurement()
{
   gettimeofday(&start, NULL);
}

void stopTimeMeasurement()
{
   gettimeofday(&ending, NULL);

    seconds  = ending.tv_sec  - start.tv_sec;
    useconds = ending.tv_usec - start.tv_usec;

    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    printf("Elapsed Time %ld ms\n", mtime);
}


/* Function name: toGray(Mat)
 * Operation: Take input RGB image of Mat type
 * returns: Mat type Grayscale image
 */
Mat toGray(Mat image)
{
	startTimeMeasurement();
	Mat gray_image(image.size(),CV_8UC1);  //declaration of empty matrix header for storing
	cvtColor( image, gray_image, CV_BGR2GRAY ); //converting
	stopTimeMeasurement();  //time to calculate the conversion time by opencv library
	return gray_image;
}

/* Function name: createHis(Mat)
 * Operation: Take input RGB or Grayscale image of Mat type
 * returns: Mat type image containing Histogram
 */
Mat createHis(Mat src)
{
	startTimeMeasurement();
	vector<Mat> bgr_vec;
	bool isRGB = true;
	if(src.channels()==1)
	{
		cout<<"Image is not RGB\n";
		isRGB = false;
	}
	else
	{
		split( src, bgr_vec );
	}
	/// Establish the number of bins. In this case 256 bins and dim =1
	int histSize = 256;
	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 } ;
	const float* histRange = { range };
    bool uniform = true; bool accumulate = false;
    Mat b_hist, g_hist, r_hist;
   /// Compute the histograms:
   if(isRGB)
   {
	  calcHist( &bgr_vec[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );
	  calcHist( &bgr_vec[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate );
	  calcHist( &bgr_vec[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate );
   }
   else
   {
	   calcHist( &src , 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );
   }
  // Draw the histograms for B, G and R
  int width_his = 1000; int height_his = 500;
  int bin_w = cvRound( (double) width_his/histSize );
  Mat hist_mat( height_his, width_his, CV_8UC3, Scalar( 0,0,0) );
  /// Normalize the result to [ 0, hist_mat.rows ]
  normalize(b_hist, b_hist, 0, hist_mat.rows, NORM_MINMAX, -1, Mat() );
  if(isRGB)
  {
	  normalize(g_hist, g_hist, 0, hist_mat.rows, NORM_MINMAX, -1, Mat() );
	  normalize(r_hist, r_hist, 0, hist_mat.rows, NORM_MINMAX, -1, Mat() );
  }

  /// Draw for each channel
  for( int i = 1; i < histSize; i++ )
  {
     line( hist_mat, Point( bin_w*(i-1), height_his - cvRound(b_hist.at<float>(i-1)) ) ,
	                 Point( bin_w*(i), height_his - cvRound(b_hist.at<float>(i)) ),
	                 Scalar( 255, 0, 0), 2, 8, 0  );
     if(isRGB)
     {
    	 line( hist_mat, Point( bin_w*(i-1), height_his - cvRound(g_hist.at<float>(i-1)) ) ,
	                     Point( bin_w*(i), height_his - cvRound(g_hist.at<float>(i)) ),
	                     Scalar( 0, 255, 0), 2, 8, 0  );
    	 line( hist_mat, Point( bin_w*(i-1), height_his - cvRound(r_hist.at<float>(i-1)) ) ,
	                     Point( bin_w*(i), height_his - cvRound(r_hist.at<float>(i)) ),
	                     Scalar( 0, 0, 255), 2, 8, 0  );
	 }
  }
  stopTimeMeasurement();
  return hist_mat;
}

/* Function name: correctGamma(Mat, double)
 * Operation: Take input img of Mat type and value of gamma of the device by which image has been taken.
 * returns: Mat type image with corrected gamma
 */
Mat correctGamma( Mat& img, double gamma )
{
	startTimeMeasurement();
	double inverse_gamma = 1.0 / gamma;
	Mat lut_matrix(1, 256, CV_8UC1 );
	uchar * ptr = lut_matrix.ptr();
	for( int i = 0; i < 256; i++ )
		ptr[i] = (int)( pow( (double) i / 255.0, inverse_gamma ) * 255.0 );
	Mat result;
	LUT( img, lut_matrix, result );
	stopTimeMeasurement();
	return result;
}

/* Function name: equilize(Mat)
 * Operation: Take input image of Mat type, which can be a grayscale or RGB image
 * returns: Mat type image with equilized image
 */
Mat equilize(Mat& img)
{
	startTimeMeasurement();
	Mat result;
	if(img.channels()==1)
	{
		equalizeHist( img, result);
	}
	else if(img.channels() >= 3)
    {
        Mat ycrcb;

        cvtColor(img,ycrcb,CV_BGR2YCrCb);

        vector<Mat> channels;
        split(ycrcb,channels);

        equalizeHist(channels[0], channels[0]);
        merge(channels,ycrcb);
        cvtColor(ycrcb,result,CV_YCrCb2BGR);
    }
	stopTimeMeasurement();
	return result;
}

/* Function name: changeContrast(Mat, int)
 * Operation: Take input image of Mat type, which can be a grayscale or RGB image
 * returns: Mat type image with changed contrast
 */

Mat changeContrast(Mat image, int alpha)
{
	startTimeMeasurement();
	Mat result;
	image.convertTo(result, -1, alpha, 0);
	/*    Alternative way
	 * Scalar alpha_val(alpha,alpha,alpha)
	 * multiply(image, alpha_val, result);
	 */
	stopTimeMeasurement();
	return result;
}

/* Function name: process_autocontrast(Mat)
 * Operation: Take input image of Mat type, which can be a grayscale or RGB image
 * returns: Mat type image with automatic contrast adjustment
 */
Mat process_autocontrast(Mat image)
{
	startTimeMeasurement();
	int maxVal=0;
	int minVal=255;
	if(image.channels()==3)
	{
		Vec3b bgrPixel;
		Mat hsv, result;
		int intensity;
		cvtColor(image,hsv,CV_BGR2HSV_FULL);
		for(int x = 0; x < hsv.rows; x++)
		{
			for(int y = 0; y < hsv.cols; y++)
			{
				//Vec3b intensity1 = hsv.at<Vec3b>(x, y);
				intensity = (int)(hsv.at<Vec3b>(x, y).val[0]);
				 if (maxVal<=intensity)
			     {
			      	maxVal= intensity;
			     }
				 if(minVal>=intensity)
				 {
				   	minVal= intensity;
				 }
			}
		}
		for(int x = 0; x < hsv.rows; x++)
	    {
			for(int y = 0; y < hsv.cols; y++)
			{
				intensity = hsv.at<Vec3b>(x, y).val[0];
				if(intensity-20<=minVal)    //change value according to extent to auto-contrast
				{
					hsv.at<Vec3b>(x, y).val[0] =0;
				}
				else if(intensity+20>=maxVal)
				{
					hsv.at<Vec3b>(x, y).val[0] =255;
				}
				else
				{
					hsv.at<Vec3b>(x, y).val[0] = (intensity-minVal)*255/(maxVal-minVal);
				}
			}
	    }
		cvtColor(hsv, result, CV_HSV2BGR_FULL);
		stopTimeMeasurement();
		return result;
	}  //if 3 channels
	else if(image.channels()==1)
	{
		Mat m;
		image.copyTo(m);
		uchar * ptr = m.ptr();
		int temp=0;
		for(int i = 0; i < m.rows*m.cols; i++)
		{
			temp = (int)ptr[i];
			if (maxVal<=temp)
			{
				maxVal= temp;
			}
			if(minVal>=temp)
			{
				minVal= temp;
			}
		}
		for(int i = 0; i < m.rows*m.cols; i++)
		{
			temp = (int)ptr[i];
			if(temp-5<=minVal)
			{
				ptr[i] = 0;
			}
			else if(temp+5>=maxVal)
			{
				ptr[i]=255;
			}
			else
			{
				ptr[i] = (temp-minVal)*255/(maxVal-minVal);
			}
		}
		stopTimeMeasurement();
		return m;
	}
	stopTimeMeasurement();
	return Mat();
}

/* Function name: median_weighted(Mat, Mat)
 * Operation: Take input image of Mat type, which can be a grayscale or RGB image and weight matrix as another argument.
 * returns: Mat type image with edges
 */
Mat median_weighted(Mat image, Mat W)
{
	const int filsize = 4;  //filter
	Mat dup;
	image.copyTo(dup);
	int pixb[2*filsize+1], pixg[2*filsize+1], pixr[2*filsize+1];
	int cntr=0;
	int row = image.rows;
	int col = image.cols;
	if(image.channels()==3)
	{
	    for(int x = 1; x < col-2; x++)
		{
			for(int y = 1; y < row-2; y++)
			{
				cntr =0;
				for (int i = -1; i <= 1; i++)
				{
					for (int j = -1; j <= 1; j++)
					{
						pixb[cntr] = dup.at<Vec3b>(y+j, x+i).val[0];
						pixg[cntr] = dup.at<Vec3b>(y+j, x+i).val[1];
						pixr[cntr] = dup.at<Vec3b>(y+j, x+i).val[2];
						cntr++;
					}
				}
				sort(pixb, pixb+ (int)(sizeof(pixb) / sizeof(pixb[0])));
				image.at<Vec3b>(y, x).val[0] = pixb[cntr];

				sort(pixg, pixg+ (int)(sizeof(pixg) / sizeof(pixg[0])));
				image.at<Vec3b>(y, x).val[1] = pixg[cntr];

				sort(pixr, pixr+ (int)(sizeof(pixr) / sizeof(pixr[0])));
				image.at<Vec3b>(y, x).val[2] = pixr[cntr];
			}
		}
		return image;
	}//channel condition

	else if(image.channels()==1)
	{    // WARNING !!! currently in debugging mode.
		for(int x = 1; x <= row-100; x++)
		{
			for(int y = 1; y <= col-500; y++)
			{
				cntr =0;
				for (int i = -1; i <= 1; i++)
				{
					for (int j = -1; j <= 1; j++)
					{
						pixb[cntr] = (int)(dup.at<uchar>(x+i, y+j));
						cntr++;
					}
				}
				sort(pixb, pixb+ (sizeof(pixb) / sizeof(pixb[0])));
				image.at<uchar>(x, y)= pixb[cntr 	];
				cout<<(int)(image.at<uchar>(x, y))<<", ";
			}
		}
		return image;
	}  //else channel 1

	return Mat();
}


/* Function name: detectEdge(Mat, int)
 * Operation: Take input image of Mat type, which can be a grayscale or RGB image
 * returns: Mat type image with edges
 */
Mat detectEdge(Mat image, int threshold)
{
  startTimeMeasurement();
  Mat mask, img_gray;
  cvtColor( image, img_gray, CV_BGR2GRAY );
  blur( img_gray, mask, Size(3,3) );

  Canny( mask, mask, threshold, threshold*3, 3 );
  Mat result;
  result = Scalar::all(0);
  image.copyTo( result, mask);
  stopTimeMeasurement();
  return result;
 }

/*
 * Function name: detectContour(Mat, int)
 * Operation: Take input image of Mat type, which can be a grayscale or RGB image
 * returns: Mat type image with contours
 */

Mat detectContour(Mat image, int threshold)
{
	startTimeMeasurement();
	Mat edge_mat, img_gray;
	cvtColor( image, img_gray, CV_BGR2GRAY );
	blur( img_gray, edge_mat, Size(3,3) );
	Canny( edge_mat, edge_mat, threshold, threshold*3, 3 );
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	findContours( edge_mat, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
	Mat drawing = Mat::zeros( edge_mat.size(), CV_8UC3 );
	Scalar color = Scalar(255,255,0);
	for( unsigned int i = 0; i< contours.size(); i++ )
	{
	     drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
	}
	stopTimeMeasurement();
	return drawing;
}

int main(int argc, char* argv[])
{

	Mat image;
	image = imread(argv[1],CV_LOAD_IMAGE_UNCHANGED);
	if( argc != 2 || !image.data )
	{
		printf( "No image data \n " );
		return -1;
	}
	string imgname = argv[1];
	double gam= 0;
	int contrast=1;
	int blur=0, lval_edge=10;  //initial value of canny edge detector
	int choice=0;
	cout<< "\n\nEnter Your choice of operation: \n\n 1. RGB to Grayscale\n 2. Create Histogram\n 3. Gamma correction\n 4. Equilize histogram\n 5. Change contrast\n 6. Auto-Contrast\n\n Filters:\n\n 7. Gaussian Filter\n 8. Median Filter(unweighted)\n\n 9. Median Filter(weighted)\n\n 10. Detect edge\n 11. Detect contour \n\n choice:";
	cin>>choice;
	switch(choice)
	{
	case 1:
		cout<<"converting image into grayscale.\n";
		imwrite("gray_"+imgname,toGray(image)); //writing to disk
		cout<<"Your result has been wrote into the file named: "<<"gray_"+imgname<<endl;
		break;
	case 2:
		cout<<"Creating Histogram.\n";
		imwrite("His_"+imgname,createHis(image)); //writing to disk
		cout<<"Your result has been wrote into the file named: "<<"His_"+imgname<<endl;
		break;

	case 3:
		cout<<"Correcting Gamma.\n Please enter the value of gamma of the device by which image has been taken (For example 2.2) :";
		cin>>gam;
		imwrite("gamma_corrected_"+imgname,correctGamma(image, gam));
		cout<<"Your result has been wrote into the file named: "<<"gamma_corrected_"+imgname<<endl;
		break;
	case 4:
		cout<<"Equilizing the histogram for te image\n";
		imwrite("equilized_"+imgname,equilize(image));
		cout<<"Your result has been wrote into the file named: "<<"equilized_"+imgname<<endl;
		break;
	case 5:
		cout<<"Changing contrast of an image\nPlease enter the contrast value:(enter 0 for default value)";
		cin>>contrast;
		if(contrast ==0)
		{
			contrast = 2;
		}
		imwrite("contrast_"+imgname,changeContrast(image, contrast));
		cout<<"Your result has been wrote into the file named: "<<"contrast_"+imgname<<endl;
		break;
	case 6:
		cout<<"Auto-contrasting the image\n";
		imwrite("autoContrast_"+imgname,process_autocontrast(image));
		cout<<"Your result has been wrote into the file named: "<<"autoContrast_"+imgname<<endl;
		break;
	case 7:
		cout<<"Gaussian Filtration of image\n Please enter the Kernel size (or the extent of bluring or smoothing an image): ";
		cin>>blur;
		startTimeMeasurement();
		GaussianBlur( image,image, Size( (2*blur)+1, (2*blur)+1 ), 0, 0 );
		stopTimeMeasurement();
		imwrite("Gaussian_"+imgname,image);
		cout<<"Your result has been wrote into the file named: "<<"Gaussian_"+imgname<<endl;
		break;
	case 8:
		cout<<"Median Filtration of image (Unweighted)\n Please enter the Kernel size (or the extent of bluring or smoothing an image): ";
		cin>>blur;
		startTimeMeasurement();
		medianBlur( image,image, (2*blur)+1);
		stopTimeMeasurement();
		imwrite("Median_"+imgname,image);
		cout<<"Your result has been wrote into the file named: "<<"Median_"+imgname<<endl;
		break;
	case 9:
		cout<<"Median Filtration of image (weighted)\n Please enter the Kernel size (or the extent of bluring or smoothing an image): ";
		cin>>blur;
		imwrite("Median_weighted_"+imgname,median_weighted(image, image));
		cout<<"Your result has been wrote into the file named: "<<"Median_weighted_"+imgname<<endl;
		break;
	case 10:
		cout<<"Edge detection of image\n Please enter the threshold value (Enter 0 for default value which is 20):";
		cin>>lval_edge;
		if(lval_edge==0)
		{
			lval_edge = 20;  //default value;
		}
		imwrite("Edge_"+imgname,detectEdge(image, lval_edge));
		cout<<"Your result has been wrote into the file named: "<<"Edge_"+imgname<<endl;
		break;
	case 11:
		cout<<"contour detection of image\n Please enter the threshold value (Enter 0 for default value which is 20):";
		cin>>lval_edge;
		if(lval_edge==0)
		{
			lval_edge = 20;
		}
		imwrite("contour_"+imgname,detectContour(image, lval_edge));
		cout<<"Your result has been wrote into the file named: "<<"contour_"+imgname<<endl;
		break;
	default:
		break;
	}
	//usleep(10);
	return 0;
}
