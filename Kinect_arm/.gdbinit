set architecture arm

define sigs
    handle SIG32 nostop noprint pass
    handle SIG33 nostop noprint pass
end

define connect
    sigs
   set solib-absolute-prefix/home/embedded/Development/Toolchain/timesys/toolchain/bin/
    target remote 134.109.5.21:222
end
