/*
 * Display.cpp
 *
 *  Created on: Jun 14, 2013
 *      Author: embedded
 */

#include "Display.h"
//#include <iostream>

Display::Display(cv::Mat mat):
	fileHand(0),
	pixelArea(0)
{
	image = mat.clone();
	for(int k= 0; k <BUF_CNT;k++)
	{
		img_buf[k] = 0;
		phys_addr[k] = 0;
	}

	// Allocating memory to  class level (ipu-handle and general IPU structiure)
	memset(&ipu_handle, 0, sizeof(ipu_lib_handle_t));
	memset(&gen_ipu, 0, sizeof(ipu_test_handle_t));

	//ipu handle feed to to general level info
	gen_ipu.ipu_handle = &ipu_handle;
	gen_ipu.mode = OP_STREAM_MODE;

	startIPU(&gen_ipu);
}

Display::~Display() {
	printf("Destructor callled!\n");
	closeIPU();
}

void Display::show(cv::Mat image)
{
	memcpy(img_buf[0],(void*)image.data,image.total()*image.elemSize());
	updateIPU();
}


int Display::startIPU(ipu_test_handle_t * gen_ipu)
{
	int ret=0;
///////////////////////////settings of input buffer/////////////////////////////////////////////////////////////////

	gen_ipu->input.width = image.cols;
	gen_ipu->input.height = image.rows;

	gen_ipu->input.fmt = v4l2_fourcc('B', 'G', 'R', '3');
	//or
	//gen_ipu->input.fmt = v4l2_fourcc('I', '4', '2', '0');
	//gen_ipu->input.fmt = v4l2_fourcc('R', 'G', 'B', '1');
	//gen_ipu->input.fmt = v4l2_fourcc('G', 'R', 'E', 'Y');

	///////////////////////////settings of output buffer/////////////////////////////////////////////////////////////////


	if ((fileHand = open("/dev/fb0", O_RDWR, 0)) < 0)
    {
		printf("Unable to open /dev/fb0\n");
		ret = -1;
	}

	if ( ioctl(fileHand, FBIOGET_VSCREENINFO, &var_screen) < 0)
	{
		printf("Get FB var info failed!\n");
		ret = -1;
	}

	gen_ipu->output.width = var_screen.xres;
	gen_ipu->output.height = var_screen.yres;
	gen_ipu->output.show_to_fb = 1;
	//gen_ipu->output.fb_disp.fb_num = 0;
	//gen_ipu->output.rot = 1;


	///////////////////////////values for general IPU/////////////////////////////////////////////////////////////////


	if (var_screen.bits_per_pixel == 24)
		gen_ipu->output.fmt = v4l2_fourcc('B', 'G', 'R', '3');
	else
		gen_ipu->output.fmt = v4l2_fourcc('R', 'G', 'B', 'P');

	pixelArea = gen_ipu->input.width * gen_ipu->input.height*3;
	ret = dma(pixelArea, BUF_CNT, phys_addr, img_buf);
	if ( ret < 0) {
		printf("dma_memory_alloc failed\n");
	}

	/* self addr in stream mode*/
	gen_ipu->input.user_def_paddr[0] = phys_addr[0];


	//filling memory according to colours
	gen_fill_pattern((char*)img_buf[0], gen_ipu->output.width, gen_ipu->output.height);

	//finally initialization of ipu
	ret = mxc_ipu_lib_task_init(&(gen_ipu->input), NULL, &(gen_ipu->output), gen_ipu->mode, gen_ipu->ipu_handle);
	if (ret < 0)
	{
		printf("mxc_ipu_lib_task_init failed!\n");

	}

	return 0;
}

int Display::updateIPU()
{
	if(mxc_ipu_lib_task_buf_update(&ipu_handle, phys_addr[0], 0, 0, NULL, NULL) < 0)
		return -1;
	else
		return 0;
}

int Display::closeIPU()
{
	mxc_ipu_lib_task_uninit(&ipu_handle);
	dma(pixelArea, BUF_CNT, phys_addr, img_buf);
	if (fileHand)
		close(fileHand);

	return 0;
}

int Display::dma(int pixelArea, int no_of_buf, dma_addr_t phys_addr[], void * vaddr[])
{
	int i, ret = 0;

	if ((fileHand = open("/dev/fb0", O_RDWR, 0)) < 0) {
		printf("Unable to open /dev/fb0\n");
		ret = -1;
		goto done;
	}
	for (i=0;i<no_of_buf;i++)
	{
		/*alloc mem from DMA zone*/
		/*input as request mem pixelArea */
		phys_addr[i] = pixelArea;
		if ( ioctl(fileHand, FBIO_ALLOC, &(phys_addr[i])) < 0)
		{
			printf("Unable alloc mem from /dev/fb0\n");
			close(fileHand);
		}

		vaddr[i] = mmap(NULL, pixelArea, PROT_READ | PROT_WRITE, MAP_SHARED,fileHand, phys_addr[i]);

		if (vaddr[i] == MAP_FAILED)
		{
			printf("mmap failed!\n");
			ret = -1;
			goto done;
		}
	}
	done:
	return ret;
}

void Display::dmf(int pixelArea, int no_of_buf, dma_addr_t phys_addr[], void * vaddr[])
{
	int i;

	for (i=0;i<no_of_buf;i++) {
		if (vaddr[i])
			munmap(vaddr[i], pixelArea);
		if (phys_addr[i])
			ioctl(fileHand, FBIO_FREE, &(phys_addr[i]));
	}
}

void Display::gen_fill_pattern(char * buf, int in_width, int in_height)
{
	int y_size = in_width * in_height;
	int h_step = in_height / 16;
	int w_step = in_width / 16;
	int h, w;
	uint32_t y_color = 0;
	int32_t u_color = 0;
	int32_t v_color = 0;
	uint32_t rgb = 0;
	static int32_t alpha = 0;
	static int inc_alpha = 1;

	for (h = 0; h < in_height; h++) {
		int32_t rgb_temp = rgb;

		for (w = 0; w < in_width; w++) {
			if (w % w_step == 0) {
				y_color = y(rgb_temp);
				y_color = (y_color * alpha) / 255;

				u_color = u(rgb_temp);
				u_color = (u_color * alpha) / 255;
				u_color += 128;

				v_color = v(rgb_temp);
				v_color = (v_color * alpha) / 255;
				v_color += 128;

				rgb_temp++;
				if (rgb_temp > 255)
					rgb_temp = 0;
			}
			buf[(h*in_width) + w] = y_color;
			if (!(h & 0x1) && !(w & 0x1)) {
				buf[y_size + (((h*in_width)/4) + (w/2)) ] = u_color;
				buf[y_size + y_size/4 + (((h*in_width)/4) + (w/2))] = v_color;
			}
		}
		if ((h > 0) && (h % h_step == 0)) {
			rgb += 16;
			if (rgb > 255)
				rgb = 0;
		}

	}
	if (inc_alpha) {
		alpha+=4;
		if (alpha >= 255) {
			inc_alpha = 0;
		}
	} else {
		alpha-=4;
		if (alpha <= 0) {
			inc_alpha = 1;
		}
	}
}


