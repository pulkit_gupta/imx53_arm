/*
 * Display.h
 *
 *  Created on: Jun 14, 2013
 *      Author: embedded
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <opencv2/core/core.hpp>

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <iostream>
#include <time.h>
#include <pthread.h>
#include <math.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <linux/mxcfb.h>
#include "mxc_ipu_hl_lib.h"

#define BUF_CNT	5
#define red(x) (((x & 0xE0) >> 5) * 0x24)
#define green(x) (((x & 0x1C) >> 2) * 0x24)
#define blue(x) ((x & 0x3) * 0x55)
#define y(rgb) ((red(rgb)*299L + green(rgb)*587L + blue(rgb)*114L) / 1000)
#define u(rgb) ((((blue(rgb)*500L) - (red(rgb)*169L) - (green(rgb)*332L)) / 1000))
#define v(rgb) (((red(rgb)*500L - green(rgb)*419L - blue(rgb)*81L) / 1000))


typedef struct {
	ipu_lib_handle_t * ipu_handle;
	int mode;
	ipu_lib_input_param_t input;
	ipu_lib_output_param_t output;
} ipu_test_handle_t;



class Display {
public:
	Display(cv::Mat mat);
	virtual ~Display();

	void show(cv::Mat image);
	int updateIPU();
	int closeIPU();
	ipu_test_handle_t gen_ipu;
private:
	int startIPU(ipu_test_handle_t * test_handle);
	int dma(int pixelArea, int no_of_buf, dma_addr_t phys_addr[], void * vaddr[]);
	void dmf(int pixelArea, int no_of_buf, dma_addr_t phys_addr[], void * vaddr[]);


	void gen_fill_pattern(char * buf, int in_width, int in_height);

	cv::Mat image;
	ipu_lib_handle_t ipu_handle;

	int fileHand;
	int pixelArea;
	void * img_buf[BUF_CNT];
	int phys_addr[BUF_CNT];

	struct fb_var_screeninfo var_screen;
	struct fb_fix_screeninfo fix_screen;

};

#endif /* IMAGEDISPLAY_H_ */
