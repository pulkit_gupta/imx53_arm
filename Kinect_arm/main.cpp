

#include <unistd.h>
#include <stdlib.h>
#include <time.h>
//#include "libfreenect.h"
//#include "libfreenect_sync.h"



#include <zmq.hpp>
#include "base64.h"
#include <pthread.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "Display.h"

#include <iostream>
#include <stdio.h>
#include <libfreenect_sync.h>
#include "ueye.h"
#include "ueye_deprecated.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <string.h>

using namespace cv;

bool ctrc = false;
Display *screen;
void sighandler(int sig);
void sighandler(int sig)
	{
	printf("Signal: %i - ", sig);

	switch(sig)
	{
		case SIGSEGV:
			printf("You idiot produced a segmentation fault!");
			exit(0);
			break;
		case SIGTERM:
			printf("Received Termination Request!");
			break;
		case SIGINT:
			ctrc = true;
			printf("Received Ctrl+C... Closing Application");
			break;
		case SIGABRT:
			printf("You idiot produced a abnormal termination!");
			break;
		case SIGFPE:
			printf("You idiot produced a floatingpoint exception!");
			break;
		case SIGILL:
			printf("Yous idiot are doing invalid instructions");
			break;
		default:
			printf("N.A.");
				break;
	}
	printf("\n");


}
Mat freenect_sync_get_depth_cv(int index)
{
	//static IplImage *image = 0;

	static char *data = 0;
	unsigned int timestamp;
	if (freenect_sync_get_depth((void**)&data, &timestamp, index, FREENECT_DEPTH_11BIT))
	    return Mat();
	Mat image(Size(640,480), CV_16UC1, data, Mat::AUTO_STEP);
	//cvSetData(image, data, 640*2);

	return image;
}
Mat freenect_sync_get_rgb_cv(int device_num)
{
	//static IplImage *image = 0;

	static char *data = 0;
	unsigned int timestamp;
	if (freenect_sync_get_video((void**)&data, &timestamp, device_num, FREENECT_VIDEO_RGB))
	    return Mat();

	Mat image(Size(640,480), CV_8UC3, data, Mat::AUTO_STEP);
	cvtColor(image, image, CV_RGB2BGR);
	//cvSetData(image, data, 640*2);
	return image;
}
Mat freenect_sync_get_ir_cv(int device_num)
{
	//static IplImage *image = 0;

	static char *data = 0;
	unsigned int timestamp;
	if (freenect_sync_get_video((void**)&data, &timestamp, device_num, FREENECT_VIDEO_IR_8BIT))
	    return Mat();
	Mat image(Size(640,480), CV_8UC1, data, Mat::AUTO_STEP);
	//cvSetData(image, data, 640*2);
	return image;
}
Mat GlViewColor(Mat depth)
{
	/*static IplImage *image = 0;
	if (!image) image = cvCreateImage(cvSize(640,480), 8, 3);*/

	Mat image(Size(640, 480), CV_8UC3);

	unsigned char *depth_mid = (unsigned char*)(image.data);
	int i;
	for (i = 0; i < 640*480; i++) {
		int lb = ((short *)depth.data)[i] % 256;
		int ub = ((short *)depth.data)[i] / 256;
		switch (ub) {
			case 0:
				depth_mid[3*i+2] = 255;
				depth_mid[3*i+1] = 255-lb;
				depth_mid[3*i+0] = 255-lb;
				break;
			case 1:
				depth_mid[3*i+2] = 255;
				depth_mid[3*i+1] = lb;
				depth_mid[3*i+0] = 0;
				break;
			case 2:
				depth_mid[3*i+2] = 255-lb;
				depth_mid[3*i+1] = 255;
				depth_mid[3*i+0] = 0;
				break;
			case 3:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 255;
				depth_mid[3*i+0] = lb;
				break;
			case 4:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 255-lb;
				depth_mid[3*i+0] = 255;
				break;
			case 5:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 0;
				depth_mid[3*i+0] = 255-lb;
				break;
			default:
				depth_mid[3*i+2] = 0;
				depth_mid[3*i+1] = 0;
				depth_mid[3*i+0] = 0;
				break;
		}
	}
	return image;
}

int cam_type = 11;
zmq::context_t context (1);
Mat depth, ir, depth2;
int x=0;
string ip_adr_rec = "tcp://199.175.49.215:8002";
string ip_adr_send = "tcp://199.175.49.215:8001";

//string ip_adr_rec = "tcp://134.109.5.21:8002";
//string ip_adr_send = "tcp://134.109.5.21:8001";

string webId = "f23_pulkit_arm";

void* rec_func(void*)
{
	string temp_check = "";
	string camera_data = "";
	printf("thread1 started");
	zmq::socket_t recieve (context, ZMQ_SUB);

	recieve.connect(ip_adr_rec.c_str());

	recieve.setsockopt(ZMQ_SUBSCRIBE, "", 0);
	while(1)
	{ 	zmq::message_t rec_msg;
		recieve.recv(&rec_msg);


		std::istringstream iss(static_cast<char*>(rec_msg.data()));
		temp_check = iss.str();
		std::cout<<"data ist "<<temp_check;
		getline(iss, temp_check, '.');
		std::cout<<"   and id recieved is  "<<temp_check<<std::endl;
		if(temp_check.compare(webId)==0)
		{
			getline(iss, camera_data, '.');
			cam_type = atoi(camera_data.c_str());
			std::cout<<"value of camera is: "<<cam_type<<std::endl;
		}
	}

}

void* show_img(void*)
{
	printf("thread2 started");
	zmq::socket_t publisher (context, ZMQ_PUB);

	publisher.connect(ip_adr_send.c_str());

    //freenect_sync_set_led((freenect_led_options)(4), 0);

	vector<int> p;
	p.push_back(CV_IMWRITE_JPEG_QUALITY);
	p.push_back(10);
	freenect_sync_set_led((freenect_led_options)(5), 0);
	screen = new Display(Mat(freenect_sync_get_ir_cv(0)));
	while (1) {

		vector<unsigned char> buffer;

	    if(cam_type == 10)
	    {
	    		depth = GlViewColor(freenect_sync_get_depth_cv(0));
	    }
	    else if(cam_type == 11)
	    {
	    	depth = freenect_sync_get_ir_cv(0);
	    }
	    else
	    {
	    	depth = freenect_sync_get_rgb_cv(0);
	    }
		 //imshow("depth", depth);
	    if(screen!=NULL)
	    	screen->show(depth);
	    else
	    	std::cout<<"SCreen not ready"<<std::endl;
	    if(!(cv::imencode(".jpg", depth, buffer, p)))
	   	{
	    	std::cout<<"can not enode image.";
	   	}
	    std::string output = base64_encode(buffer.data(), buffer.size());
	    //std::cout<<output.c_str();
	    output= webId+"."+output;
	    zmq::message_t msg(output.length());
	    memcpy ((void *) msg.data (), output.c_str(), output.length());
	   publisher.send(msg);
	    //usleep(1000000);   //1000*1000 = 1 sec 250000 was optimal for pc
		//i++;
	}
}

int main(int argc, char **argv)
{
	signal(SIGABRT, &sighandler);
		signal(SIGTERM, &sighandler);
		signal(SIGINT, &sighandler);
		signal(SIGSEGV, &sighandler);
		signal(SIGABRT, &sighandler);
		signal(SIGFPE, &sighandler);
	printf("program started");

	pthread_t thread[2];
	int rc;
	rc = pthread_create(&thread[0], NULL,&show_img, NULL);
	rc = pthread_create(&thread[1], NULL,&rec_func, NULL);
	if (rc){
	         std::cout << "Error:Unable to create thread," << rc << std::endl;
	         exit(-1);
	      }

	while(!ctrc)
	{

	}

	return 0;
}
