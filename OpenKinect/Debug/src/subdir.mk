################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/OpenKinect.cpp \
../src/base64.cpp 

C_SRCS += \
../src/libfreenect_sync.c 

OBJS += \
./src/OpenKinect.o \
./src/base64.o \
./src/libfreenect_sync.o 

C_DEPS += \
./src/libfreenect_sync.d 

CPP_DEPS += \
./src/OpenKinect.d \
./src/base64.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/home/embedded/Development/Libs/opencv-2.4.5_build_linux/lib -I/usr/include/ -I/usr/local/include/libfreenect -I/usr/local/include/ -I/usr/include/libusb-1.0 -I/home/embedded/Development/Libs/opencv-2.4.5_build/install/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I/usr/local/include/libfreenect -I/usr/include/libusb-1.0 -I/usr/local/include -I/usr/include/ -I/home/embedded/Development/Libs/opencv-2.4.5_build/install/include -I/home/embedded/Development/Libs/opencv-2.4.5_build_linux/install/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


